<?php

class Hangman {
  private $intLives = 6;
  private $arrWords = array('antidisestablishmentarianism', 'bikes', 'cheeseburgers', 'crackerjack', 'fusion', 'mammalian');
  private $arrAllowedCharacters = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
  private $txtCurrentWord = '';
  private $txtCurrentGuessedWord = '';
  private $arrAtemptedCharacters = array();
  
  function StartGame() {
    $this->intLives = 6;
    $this->txtCurrentGuessedWord = '';
    $this->txtCurrentWord = '';
    
    $this->arrAtemptedCharacters = array();
    $intRandomIndex = array_rand($this->arrWords, 1);
    $this->txtCurrentWord = $this->arrWords[$intRandomIndex];
  }
  
  function PlayMove($_POST) {
    $txtOutput = '';
    
    if (!$this->txtCurrentWord) {
      $this->StartGame();
    }
    
    if (isset($_POST['reset'])) {
      $this->StartGame();
    }
    else {
      if (isset($_POST) && isset($_POST['txtCharacter'])) {
        $txtOutput = $this->AttemptCharacter($_POST['txtCharacter']);
      }
    }
    $this->FindCharacter();
    if ($this->txtCurrentWord == str_replace(' ', '', $this->txtCurrentGuessedWord)) {
      $txtOutput .= $this->CompleteGame(TRUE);
    }
    $txtOutput .= $this->GetHTML();
    
    return $txtOutput;
  }

  function AttemptCharacter($txtAttemptCharacter) {
    $txtOutput = '';
    
    if (!in_array($txtAttemptCharacter, $this->arrAllowedCharacters)) {
      $txtOutput .= $this->ReturnMessage('Character not allowed, only A-Z charset is allowed.', 'error');
    }
    else {
      if (in_array($txtAttemptCharacter, $this->arrAtemptedCharacters)) {
        $txtOutput .= $this->ReturnMessage("You have already attempted the letter '" . $txtAttemptCharacter . "'", 'error');
      }
      else {
        $this->arrAtemptedCharacters[] = $txtAttemptCharacter;

        if (strpos($this->txtCurrentWord, $txtAttemptCharacter) !== FALSE) {
          $txtOutput .= $this->ReturnMessage('Letter has been found');
        }
        else {
          $txtOutput .= $this->RemoveLife();
        }
      }
    }
    return $txtOutput;
  }
  
  function RemoveLife() {
    $txtOutput = '';
    $this->intLives--;
    if (!$this->intLives) {
      $txtOutput .= $this->CompleteGame();
    }
    else {
      $txtOutput .= $this->ReturnMessage('Letter was <strong>not</strong> found');
    }
    return $txtOutput;
  }
  
  function ReturnMessage($txtMessage, $txtType = 'status') {
    return '<div class="message' . $txtType . '">' . $txtMessage . '</div>'; 
  }
  
  function GetPicture() {
    return '<img src="images/stage-' . $this->intLives . '.png">'; 
  }
  
  function CompleteGame($blnOutcome = FALSE) {
    $txtWord = $this->txtCurrentWord;
    $this->StartGame();
    if ($blnOutcome) {
      return $this->ReturnMessage("Game Won! You found: '" . $txtWord . "'");
    }
    return $this->ReturnMessage('Game Lost', 'error');
  }
  
  function GetHTML() {
    return 'Current Word: ' . $this->FindCharacter();
  }
  
  function FindCharacter() {
    $arrIndex = array();
    $intLength = strlen($this->txtCurrentWord);
    $this->txtCurrentGuessedWord = '';
    for($intCurrentIndex = 0; $intCurrentIndex < $intLength; $intCurrentIndex++) {
      $txtCurrentLetter = $this->txtCurrentWord{$intCurrentIndex};
      if (in_array($txtCurrentLetter, $this->arrAtemptedCharacters)) {
        $this->txtCurrentGuessedWord .= $txtCurrentLetter . ' ';
      }
      else {
        $this->txtCurrentGuessedWord .= '_ ';
      }
    }
    return trim($this->txtCurrentGuessedWord);
  }
  
  function GetAttemptedLetters() {
    return 'The following letters have been used: ' . implode(', ', $this->arrAtemptedCharacters);
  }
}