<?php
session_start();

function __autoload($name) {
  require_once 'classes/' . $name . '.class.php';
}

if (!isset($_SESSION['han001'])) {
  $_SESSION['han001'] = array();
}
if (!isset($_SESSION['han001']['game'])) {
  $_SESSION['han001']['game'] = new Hangman();
}
?>
<html>
  <head>
    <title>han001 - Hangman, Fluid Creativity</title>
    <style>
      .messages div {
        padding:10px;
        border:1px solid;
        margin-bottom:10px;
      }
      .messagestatus {
        background-color: #F8FFF0;
        color: #234600;
        border-color:1px solid #BBEE77;
      }
      
      .messageerror {
        background-color: #FEF5F1;
        color: #8C2E0B;
        border-color: #ED541D;
      }
    </style>
  </head>
  <body>
    <div id="content">
      <form method="post">
        <div class="messages">
          <?php echo $_SESSION['han001']['game']->PlayMove($_POST); ?>
        </div>
        <div class="picture"><?php print $_SESSION['han001']['game']->GetPicture(); ?></div>
        <input type="text" name="txtCharacter" id="txtCharacter" value="" size="150" />
        <input type="submit" value="Guess" /><input type="submit" name="reset" value="Reset" />
      </form>
      <div class="letters-attempted"><?php print $_SESSION['han001']['game']->GetAttemptedLetters(); ?></div>
    </div>
    <script>
      document.getElementById('txtCharacter').focus();
    </script>
  </body>
</html>